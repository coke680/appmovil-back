<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
    protected $fillable = ['id_client', 'status'];

    public function client()
    {
        return $this->belongsTo(Client::class, 'id_client');
    }
}
