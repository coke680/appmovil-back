<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Client;
use Session;
use App\Http\Controllers\Controller;
use JWTAuth;
use JWTAuthException;

class ClientController extends Controller
{
    public function index()
    {
        $clientes = DB::table('clients')->select('id', 'client_number as number',
                    'name', 'last_name','address','city_address', 'contact')
                    ->get();

        return view('clients.index', ['clientes' => $clientes]);
    }

    public function store(Request $request) {

        $this->validate($request, [
            'client_number' => 'required|integer|min:4|unique:clients',
            'name' => 'required|max:200|min:4',
            'last_name' => 'required|max:200|min:4',
            'address' => 'required|max:200|min:4',
            'city_address' => 'required|max:200|min:4',
            'contact' => 'required|max:200|min:6',
            'password' => 'required|min:6|confirmed',
       
        ]);

        Client::create([
            'client_number' => $request['client_number'],
            'name' => $request['name'],
            'last_name' => $request['last_name'],
            'address' => $request['address'],
            'city_address' => $request['city_address'],
            'contact' => $request['contact'],
            'password' => bcrypt($request['password']),
        ]);

        Session::flash('success', 'Cliente creado exitosamente');
        return redirect()->to('clients');
    }


    public function edit($id){

        $client = Client::find($id);
        //return view('clients.edit', compact('clients', 'cliente'));
        return view('clients.edit', compact('client'));
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'client_number' => 'required|integer|min:4',
            'name' => 'required|max:200|min:4',
            'last_name' => 'required|max:200|min:4',
            'address' => 'required|max:200|min:4',
            'city_address' => 'required|max:200|min:4',
            'contact' => 'required|max:200|min:6',
        ]);

        $client = Client::find($id);

        $client->update(request()->all());
        Session::flash('success', 'Cliente editado exitosamente');
        return redirect()->to('clients');
    }


    public function destroy($id) {
        try {
            $client = Client::find($id);

            $client->delete();
            Session::flash('warning', 'Cliente eliminado exitosamente');
            return back();
        }
        catch(\Illuminate\Database\QueryException $e) {
            Session::flash('error', 'Error. Cliente no puede ser eliminado');
            return back();
        }
    }

}
