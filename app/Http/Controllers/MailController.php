<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Log;
use DB;

class MailController extends Controller
{
    public function sendemail() {
    	
        $notification = DB::table('activations')->select('id','status', 'id_client as client_id',
                    'created_at', 'updated_at')
                    ->get();
    	//$notification = 'Jorge Martinez';
    	$data = ['notification' => $notification];

        Mail::send('emails.notification', $data, function($message) {
            $message->to('cristiansnake1990@gmail.com')
            
            ->subject('Alerta de vencimiento de Documentos');
        });

        Log::info('Mail enviado');
    }
}
