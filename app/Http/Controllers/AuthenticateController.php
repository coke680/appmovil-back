<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;

class AuthenticateController extends Controller
{

    public function __construct() {
         // Apply the jwt.auth middleware to all methods in this controller
         // except for the authenticate method. We don't want to prevent
         // the user from retrieving their token if they don't already have it
         $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

  	public function index() {
  	    // Retrieve all the users in the database and return them
  	    /*$users = User::all();
  	    return $users;*/
  	}  

    public function authenticate(Request $request)	{
      	
          $credentials = $request->only('client_number', 'password');

          try {
              // verify the credentials and create a token for the user
              if (! $token = JWTAuth::attempt($credentials)) {
                  return response()->json(['error' => 'invalid_credentials'], 401);
              }
          } catch (JWTException $e) {
              // something went wrong
              return response()->json(['error' => 'could_not_create_token'], 500);
          }

          // if no errors are encountered we can return a JWT
          return response()->json(compact('token'));
    }

    public function getAuthenticatedUser() {
      try {

          if (! $user = JWTAuth::parseToken()->authenticate()) {
              return response()->json(['user_not_found'], 404);
          }

          } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

          } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

          } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());

          }

      $user = \App\User::where('client_number', $user->client_number)->first();

      $last = \App\Activation::where('id_client', $user->id)->orderBy('created_at', 'DESC')->first();

      if ($last == null) {

        $status = 0;

        return response()->json(compact('status', 'user', 'report'));
      }

      else {

        $status = $last->status;

        return response()->json(compact('status', 'user', 'report'));
      }

    }

    public function getReportUser() {
      try {

          if (! $user = JWTAuth::parseToken()->authenticate()) {
              return response()->json(['user_not_found'], 404);
          }

          } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

          } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

          } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());

          }


      $report = \App\Activation::where('id_client', $user->id)->orderBy('created_at', 'DESC')->limit(10)->get();

      return response()->json(compact('report'));
    }
}