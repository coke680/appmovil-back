<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
use App\Activation;
use DB;

class ActivationController extends Controller
{
    public function index() {

        $activations = DB::table('activations')->select('activations.id','id_client as client',
                    'status', 'activations.created_at as date', 
                    DB::raw("CONCAT(clients.name,' ',clients.last_name)  as client_name"), 'client_number')
                    ->join('clients', 'clients.id', '=', 'activations.id_client')->latest('activations.created_at')
                    ->get();

        return view('activations.index', ['activations' => $activations]);

    }
    public function store(Request $request) {

    	$detail = Activation::create([
            'id_client' => \JWTAuth::authenticate()->id,
            'status' => $request['status'],
        ]);

        $data = ['detail' => $detail];

        Mail::send('emails.notification', $data, function($message) {
            $message->to('cristiansnake1990@gmail.com')
            
            ->subject('Alerta de monitoreo');
        });

    }

    public function destroy($id) {
        try {
            $activation = Activation::find($id);

            $activation->delete();
            Session::flash('warning', 'Registro eliminado');
            return back();
        }
        catch(\Illuminate\Database\QueryException $e) {
            Session::flash('error', 'Error. Registro no pudo ser eliminado');
            return back();
        }
    }
}
