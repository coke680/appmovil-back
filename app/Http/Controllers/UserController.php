<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\User;
use Session;
use Auth;

class UserController extends Controller
{
    // public function __construct(){
        
    //     $this->middleware('auth');
    // }
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function index()
    {
        $users = DB::select('select * from users ORDER BY (created_at)');

        return view('users.index', ['users' => $users]);
    }

    public function create()
    {
        return view('users.register');
    }

    public function store(Request $request)
    {   
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],            
            'password' => bcrypt($request['password']),
        ]);

        Session::flash('success', 'Usuario creado exitosamente');
        return redirect()->to('users');
    }

    public function edit($id) {

        $user = User::find($id);

        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $id) {

        $user = User::find($id);

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            'password' => 'required|min:6|confirmed',
        ]);

        $user->update(request()->all());

        Session::flash('warning', 'Usuario editado exitosamente');
        return redirect()->to('users');
    }

    public function destroy($id) {

        try {
            $user = User::find($id);

            if($id == Auth::user()->id){
                Session::flash('error', 'Error. Usuario no puede eliminarse a sí mismo');
                return back();                       
            }

           if ($user != null){ 
            
                $user->delete(); 
                Session::flash('warning', 'Usuario eliminado exitosamente');
                return back();
            }
            
        }
        catch(\Illuminate\Database\QueryException $e) {
            Session::flash('error', 'Error. Usuario no eliminado');
            return back();
        }
    }
}
