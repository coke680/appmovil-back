    <!-- modal add cliente -->
    <div class="modal fade" id="modal-create">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Ingreso de nuevo cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ url('/clients') }}" method="POST" role="form">

                                {{ csrf_field() }}

                                <div class="form-group {{ $errors->has('client_number') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control" required id="client_number" value="{{ old('client_number') }}" name="client_number" placeholder="Número de cliente">
                                    @if ($errors->has('client_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('client_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control" required id="name" name="name" value="{{ old('name') }}" placeholder="Nombres">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control" required id="last_name" name="last_name" value="{{ old('last_name') }}" placeholder="Apellidos">
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control" required id="address" name="address" value="{{ old('address') }}" placeholder="Dirección">
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('city_address') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control" required id="city_address" name="city_address" value="{{ old('city_address') }}" placeholder="Ciudad">
                                    @if ($errors->has('city_address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('city_address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control" required id="contact" name="contact" value="{{ old('contact') }}" placeholder="Contacto">
                                    @if ($errors->has('contact'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('contact') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input type="password" class="form-control" required id="password" name="password" value="{{ old('password') }}" placeholder="Ingrese password para aplicación móvil">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">      
                                    <input id="password-confirm" type="password" placeholder="Confirme password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif                                   
                                </div>
                                
                                <div class="modal-footer">                    
                                    <button type="submit" class="btn btn-primary">Guardar cliente</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                
            </div>
        </div>
    </div>