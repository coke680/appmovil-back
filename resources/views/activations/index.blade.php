@extends ('layouts.app')

@section('content')

    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('warning') }}</strong>
        </div>
    @endif

    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif

	<h3>Listado de activaciones</h3><hr>
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="activationsTable">
			<thead>
				<tr>
					<th>Fecha</th>
                    <th>Número de cliente</th>
					<th>Nombre cliente</th>
                    <th>Estado de la activación</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
			@foreach($activations as $activation)
				<tr>
					<td>{{ $activation->date }}</td>
                    <td>{{ $activation->client_number }}</td>
					<td>{{ $activation->client_name }}</td>
                    <td>{{ $activation->status ? 'Activado' : 'Desactivado'  }}</td>
					<td><a href="activations/del/{{ $activation->id }}" onclick="return confirm('¿Está seguro de eliminar este registro?')" class="btn btn-xs btn-danger">Eliminar</a></td> 
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
    
@endsection

@push('datatable')
  <script>
    $(document).ready(function(){
        $('#activationsTable').DataTable({

            responsive: true,
            processing: true,
            bLengthChange: false,
            order: [[ 0, "desc" ]],

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },
            dom: 'Bfrtip',

            columnDefs: [
                { width: 10, targets: 0 },
                { width: 100, targets: 1 },
                { width: 100, targets: 2 },
                { width: 100, targets: 3 },
                { width: 100, targets: 4 },
            ],
            fixedColumns: true,

        });

    });
</script>
@endpush