<?php

header('Access-Control-Allow-Origin', '*');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization, X-Requested-With');

//DEJAR ESTA RUTA EN EL GROPU LUEGO DE IMPLEMENTAR LA SESIÓN


Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\AdminLoginController@login');
Route::post('logout', 'Auth\AdminLoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function(){

	Route::get('/', function () {
	    return redirect('/activations');
	});

	//RUTAS DE CLIENTES
	Route::get('/clients', 'ClientController@index');
	Route::get('/clients/create', 'ClientController@create');
	Route::post('/clients', 'ClientController@store');
	Route::get('/clients/del/{id}', ['uses' => 'ClientController@destroy']);
	Route::get('/clients/edit/{id}', ['uses' => 'ClientController@edit']);
	Route::post('/clients/{id}', ['uses' => 'ClientController@update']);

	//RUTAS DE USUARIOS
	Route::get('/users', 'UserController@index');
	Route::get('/users/create', 'UserController@create');
	Route::post('/users', 'UserController@store');
	Route::get('/users/del/{id}', ['uses' => 'UserController@destroy']);
	Route::get('/users/edit/{id}', ['uses' => 'UserController@edit']);
	Route::post('/users/{id}', ['uses' => 'UserController@update']);

	//RUTAS DE activaciones
	Route::get('/activations', 'ActivationController@index');
	Route::get('/activations/create', 'ActivationController@create');
	
	Route::get('/activations/del/{id}', ['uses' => 'ActivationController@destroy']);
	/*Route::get('/activations/edit/{id}', ['uses' => 'ActivationController@edit']);
	Route::post('/activations/{id}', ['uses' => 'ActivationController@update']);*/   

});

Route::group(['prefix' => 'api'], function()
{
    Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('authenticate', 'AuthenticateController@authenticate');
});

Route::group(['middleware' => 'jwt.auth'], function(){
	Route::post('/activations', 'ActivationController@store');
	Route::get('api/authenticate/user', 'AuthenticateController@getAuthenticatedUser');
    Route::get('api/authenticate/report', 'AuthenticateController@getReportUser');
});
